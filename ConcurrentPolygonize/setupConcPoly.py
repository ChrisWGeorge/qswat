from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import os
import numpy
includePath = os.environ['OSGEO4W_ROOT'] + r'/apps/Python27/include'
if os.environ.has_key('CPATH'):
    os.environ['CPATH'] = os.environ['CPATH'] + ';' + includePath + ';' + numpy.get_include()
else:
    os.environ['CPATH'] = includePath + ';' + numpy.get_include()
if os.environ.has_key('PATH'):
    os.environ['PATH'] = os.environ['PATH'] + ';C:/MinGW/bin'
else:
    os.environ['PATH'] = 'C:/MinGW/bin'
ext_modules = [
    Extension(
        "ConcPolygonize",
        ["ConcPolygonize.pyx"],
        extra_compile_args=['-fopenmp'],
        extra_link_args=['-fopenmp'],
        include_path=[os.environ['CPATH']],
        )
    ]
setup(
    name = "Polygonize",
    package_dir = {'QSWAT': ''}, 
    ext_modules = cythonize(ext_modules),
)