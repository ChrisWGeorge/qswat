# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSWAT
                                 A QGIS plugin
 Create SWAT inputs
                              -------------------
        begin                : 2014-07-18
        copyright            : (C) 2014 by Chris George
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *  # @UnusedWildImport
from PyQt4.QtGui import * # @UnusedWildImport
from PyQt4.QtXml import * # @UnusedWildImport
from qgis.core import * # @UnusedWildImport
import os.path
import posixpath
import ntpath
import glob
import shutil
import random
import datetime
import sys
from osgeo import gdal, ogr
import traceback

class QSWATUtils:
    """Various utilities."""
    
    _SLOPE_GROUP_NAME = 'Slope'
    _LANDUSE_GROUP_NAME = 'Landuse'
    _SOIL_GROUP_NAME = 'Soil'
    _WATERSHED_GROUP_NAME = 'Watershed'
    _RESULTS_GROUP_NAME = 'Results'
    _ANIMATION_GROUP_NAME = 'Animations'
    
    _SNAPPEDLEGEND = 'Snapped inlets/outlets'
    _SELECTEDLEGEND = 'Selected inlets/outlets'
    _DRAWNLEGEND = 'Drawn inlets/outlets'
    _EXTRALEGEND = 'Extra inlets/outlets'
    _FULLHRUSLEGEND = 'Full HRUs'
    _ACTHRUSLEGEND = 'Actual HRUs'
    _HILLSHADELEGEND = 'Hillshade'
    _WATERSHEDLEGEND = 'Watershed'
    _GRIDLEGEND = 'Watershed grid'
    _GRIDSTREAMSLEGEND = 'Grid streams'
    
    @staticmethod
    def error(msg, isBatch):
        """Report msg as an error."""
        QSWATUtils.logerror(msg)
        if isBatch:
            # in batch mode we generally only look at stdout 
            # (to avoid distracting messages from gdal about .shp files not being supported)
            # so report to stdout
            sys.stdout.write('ERROR: {0}\n'.format(msg))
        else:
            msgbox = QMessageBox()
            msgbox.setWindowTitle('QSWAT')
            msgbox.setIcon(QMessageBox.Critical)
            msgbox.setText(QSWATUtils.trans(msg))
            msgbox.exec_()
        return
    
    @staticmethod
    def question(msg, parent, affirm):
        """Ask msg as a question, returning Yes or No."""
        # only ask question if interactive (not eg testing)
        if parent and parent.isVisible():
            questionBox = QMessageBox()
            questionBox.setWindowTitle('QSWAT')
            questionBox.setIcon(QMessageBox.Question)
            questionBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            questionBox.setText(QSWATUtils.trans(msg))
            result = questionBox.exec_()
        else: # not interactive: use affirm parameter
            if affirm:
                result = QMessageBox.Yes
            else:
                result = QMessageBox.No
        if result == QMessageBox.Yes:
            res = ' Yes'
        else:
            res = ' No'
        QSWATUtils.loginfo(msg + res)
        if not (parent and parent.isVisible()):
            sys.stdout.write('{0}\n'.format(msg + res))
        return result
    
    @staticmethod
    def information(msg, isBatch):
        """Report msg as information."""
        QSWATUtils.loginfo(msg)
        if isBatch:
            sys.stdout.write('{0}\n'.format(msg))
        else:
            msgbox = QMessageBox()
            msgbox.setWindowTitle('QSWAT')
            msgbox.setIcon(QMessageBox.Information)
            msgbox.setText(QSWATUtils.trans(msg))
            msgbox.exec_()
        return
    
    @staticmethod
    def exceptionError(msg, isBatch):
        """Report exception as error."""
        QSWATUtils.error('{0}: {1}'.format(msg, traceback.format_exc()), isBatch)
        return
    
    @staticmethod
    def loginfo(msg):
        """Log message as information."""
        QgsMessageLog.logMessage(msg, 'QSWAT', QgsMessageLog.INFO)
        
    @staticmethod
    def logerror(msg):
        """Log message as error."""
        QgsMessageLog.logMessage(msg, 'QSWAT', QgsMessageLog.CRITICAL)
        
    @staticmethod
    def trans(msg):
        """Translate msg according to current locale."""
        return QApplication.translate("QSwat", msg, None, QApplication.UnicodeUTF8)
    
    @staticmethod
    def join(path, fileName):
        """Use appropriate path separator."""
        if os.name == 'nt':
            return ntpath.join(path, fileName)
        else:
            return posixpath.join(path, fileName)
        
    @staticmethod
    def relativise(filePath, path):
        """If filePath exists, return filePath relative to path, else return empty string."""
        if os.path.exists(filePath):
            return os.path.relpath(filePath, path)
        else:
            return ''
        
    @staticmethod
    def samePath(p1, p2):
        """Return true if absolute paths of the two paths are the same."""
        return os.path.abspath(p1) == os.path.abspath(p2)
    
    @staticmethod
    def copyPrj(inFile, outFile):
        """
        Copy .prj file, if it exists, from inFile to .prj file of outFile,
        unless outFile is .dat.
        """
        inBase = os.path.splitext(inFile)[0]
        (outBase, outSuffix) = os.path.splitext(outFile)
        if not outSuffix == '.dat':
            inPrj = inBase + '.prj'
            outPrj = outBase + '.prj'
            if os.path.exists(inPrj):
                shutil.copy(inPrj, outPrj)
            
    @staticmethod
    def isUpToDate(inFile, outFile):
        """Return true if inFile exists, outFile exists and is no older than inFile."""
        if not os.path.exists(inFile):
            return False
        if os.path.exists(outFile):
            if os.path.getmtime(outFile) >= os.path.getmtime(inFile):
                return True
        return False
    
    @staticmethod
    def progress(text, label):
        """Set label text and repaint."""
        if text == '':
            label.clear()
        else:
            label.setText(text)
            # shows on console if visible; more useful in testing when appears on standard output
            print text
        label.repaint()
        QCoreApplication.processEvents()
        
    @staticmethod
    def layerFileInfo(layer):
        """Return QFileInfo of raster or vector layer."""
        provider = layer.dataProvider()
        if isinstance(layer, QgsRasterLayer):
            return QFileInfo(provider.dataSourceUri())
        elif isinstance(layer, QgsVectorLayer):
            path  = provider.dataSourceUri()
            # vector data sources have additional "|layerid=0"
            pos = path .find('|')
            if pos >= 0:
                path  = path [:pos]
            return QFileInfo(path)
        return None
      
    @staticmethod
    def removeLayerAndFiles(fileName, li):
        """Remove any layers for fileName; delete files with same basename 
        regardless of suffix.
        """
        QSWATUtils.removeLayer(fileName, li)
        QSWATUtils.removeFiles(fileName)
      
    @staticmethod
    def tryRemoveLayerAndFiles(fileName, li):
        """Remove any layers for fileName; delete files with same basename 
        regardless of suffix, but allow deletions to fail.
        """
        QSWATUtils.removeLayer(fileName, li)
        QSWATUtils.tryRemoveFiles(fileName)
          
    @staticmethod  
    def removeLayer(fileName, li):
        """Remove any layers for fileName."""
        fileInfo = QFileInfo(fileName)
        lIds = []
        for layer in li.layers():
            info = QSWATUtils.layerFileInfo(layer)
            if info == fileInfo:
                lIds.append(layer.id())
        QgsMapLayerRegistry.instance().removeMapLayers(lIds)
     
    @staticmethod
    def removeLayerByLegend(legend, layers):
        """Remove any layers whose legend name starts with the legend."""
        lIds = []
        for layer in layers:
            name = layer.name()
            if name.startswith(legend):
                lIds.append(layer.id())
        QgsMapLayerRegistry.instance().removeMapLayers(lIds)
        
    @staticmethod
    def getLayerByLegend(legend, layers):
        """Find a layer if any whose legend name starts with the legend."""
        for layer in layers:
            if layer.name().startswith(legend):
                return layer
        return None
    
    @staticmethod
    def getLayersByGroup(group, legendInterface, visible=False):
        """Return list of layers in group, restricted to visible if visible is true."""
        infos = legendInterface.groupLayerRelationship()
        for grp, ids in infos:
            if grp == group:
                layers = [QSWATUtils.getLayerById(lid, legendInterface.layers()) for lid in ids]
                if visible:
                    return [layer for layer in layers if legendInterface.isLayerVisible(layer)]
                else:
                    return layers
        return []
    
    @staticmethod
    def countLayersInGroup(group, legendInterface):
        """Return number of layers in group."""
        infos = legendInterface.groupLayerRelationship()
        for grp, ids in infos:
            if grp == group:
                return len(ids)
        return 0
    
    @staticmethod
    def getLayerById(lid, layers):
        """Find layer by id, and raise exception if not found."""
        for layer in layers:
            if layer.id() == lid:
                return layer
        raise ValueError(u'Cannot find layer with identifier {0}'.format(lid))         
                
    @staticmethod
    def removeFiles(fileName):
        """
        Delete all files with same root as fileName, 
        i.e. regardless of suffix.
        """
        pattern = os.path.splitext(fileName)[0] + '.*'
        for f in glob.iglob(pattern):
            os.remove(f)
                
    @staticmethod
    def tryRemoveExtendedFiles(fileName, suffix=''):
        """
        Try to delete all files with same root as fileName, 
        i.e. regardless of suffix unless provided, and including extension, eg numbers.
        
        If the filename supplied is X.Y, this will try to remove 
        all files matching X*.*.
        If the filename supplied is X.Y, and Z is the suffix argument this will try to remove 
        all files matching X*.Z.  Y can be the same as Z: it is ignored.
        """
        base = os.path.splitext(fileName)[0]
        if suffix != '' and not suffix.startswith('.'):
            suffix = '.' + suffix
        pattern = base + '*.*' if suffix == '' else base + '*' + suffix
        for f in glob.iglob(pattern):
            try:
                os.remove(f)
            except Exception:
                pass
                
    @staticmethod
    def tryRemoveFiles(fileName):
        """
        Delete all files with same root as fileName, 
        i.e. regardless of suffix, but allow deletions to fail.
        """
        pattern = os.path.splitext(fileName)[0] + '.*'
        for f in glob.iglob(pattern):
            try:
                os.remove(f)
            except Exception:
                pass
            
    @staticmethod
    def copyFiles(inInfo, saveDir):
        """
        Copy files with same basename as file with info  inInfo to saveDir, 
        i.e. regardless of suffix.
        """
        inFile = inInfo.fileName()
        inPath = inInfo.path()
        if inFile == 'sta.adf' or inFile == 'hdr.adf':
            # ESRI grid: need to copy top directory of inInfo to saveDir
            inDirName = inInfo.dir().dirName()
            savePath = QSWATUtils.join(saveDir, inDirName)
            # guard against trying to copy to itself
            inPath = os.path.normcase(os.path.abspath(inPath))
            savePath = os.path.normcase(os.path.abspath(savePath))
            if inPath != savePath:
                if os.path.exists(savePath):
                    shutil.rmtree(savePath)
                shutil.copytree(inPath, savePath, True, None)
        else:
            pattern = QSWATUtils.join(inPath, inInfo.baseName()) + '.*'
            for f in glob.iglob(pattern):
                shutil.copy(f, saveDir)
                
    @staticmethod
    def copyShapefile(infile, outbase, outdir):
        """Copy files with same basename as infile to outdir, setting basename to outbase."""
        pattern = os.path.splitext(infile)[0] + '.*'
        for f in glob.iglob(pattern):
            suffix = os.path.splitext(f)[1]
            outfile = QSWATUtils.join(outdir, outbase + suffix)
            shutil.copy(f, outfile)
            
    @staticmethod
    def nextFileName(baseFile, n):
        """If baseFile takes form X.Y, returns Xz.Y, z where z is the smallest integer >= n such that Xz.Y does not exist."""
        base, suffix = os.path.splitext(baseFile)
        nextFile = base + str(n) + suffix
        while os.path.exists(nextFile):
            n += 1
            nextFile = base + str(n) + suffix
        return nextFile, n
                
    @staticmethod
    def getLayerByFilenameOrLegend(layers, fileName, ft, legend, parent):
        """Look for file that should have a layer.  
        If not found by filename, try legend, either as given or by file type ft."""
        layer = QSWATUtils.getLayerByFilename(layers, fileName, ft, None, False)[0]
        if layer:
            return layer
        lgnd = FileTypes.legend(ft) if legend == '' else legend
        layer = QSWATUtils.getLayerByLegend(lgnd, layers)
        if layer:
            possFile = QSWATUtils.layerFileInfo(layer).absoluteFilePath()
            if QSWATUtils.question('Use {0} as {1} file?'.format(possFile, lgnd), parent, True) == QMessageBox.Yes:
                return layer
        return None
            
    @staticmethod        
    def getLayerByFilename(layers, fileName, ft, gv, doLoad):
        """
        Return layer for this fileName and flag to indicate if new layer, 
        loading it if necessary if doLoad is true.
        """
        fileInfo = QFileInfo(fileName)
        for layer in layers:
            if QSWATUtils.layerFileInfo(layer) == fileInfo:
                return (layer, False)
        # not found: load layer if requested
        if doLoad:
            legend = FileTypes.legend(ft)
            styleFile = FileTypes.styleFile(ft)
            baseName = fileInfo.baseName()
            if fileInfo.suffix() == 'adf':
                # ESRI grid: use directory name as baseName
                baseName = fileInfo.dir().dirName()
            if ft == FileTypes._OUTLETS:
                if baseName.endswith('_snap'):
                    legend = QSWATUtils._SNAPPEDLEGEND
                elif baseName.endswith('_sel'):
                    legend = QSWATUtils._SELECTEDLEGEND
                elif baseName.endswith('extra'):  # note includes arcextra
                    legend = QSWATUtils._EXTRALEGEND
                elif baseName == ('drawoutlets'):
                    legend = QSWATUtils._DRAWNLEGEND
            # some file types like MASK may be vector or raster
            if QgsRasterLayer.isValidRasterFileName(fileName):
                layer = QgsRasterLayer(fileName, '{0} ({1})'.format(legend, baseName))
            else:
                ogr.RegisterAll()
                layer = QgsVectorLayer(fileName, '{0} ({1})'.format(legend, baseName), 'ogr')
            if layer and layer.isValid():
                QSWATUtils.removeLayerByLegend(legend, layers)
                layer = QgsMapLayerRegistry.instance().addMapLayer(layer)
                fun = FileTypes.colourFun(ft)
                if fun: 
                    fun(layer, gv.db)
                if not (styleFile is None or styleFile == ''):
                    layer.loadNamedStyle(QSWATUtils.join(gv.plugin_dir, styleFile))
                # save qml form of DEM style file if batch (no support for sld form for rasters)
                if gv.isBatch and ft == FileTypes._DEM:
                    qmlFile = QSWATUtils.join(gv.projDir, 'dem.qml')
                    (msg, OK) = layer.saveNamedStyle(qmlFile)
                    if not OK:
                        QSWATUtils.error('Failed to create dem.qml: {0}'.format(msg), gv.isBatch)
                return (layer, True)
            else:
                QSWATUtils.error('Failed to load {0}'.format(fileName), gv.isBatch)
        return (None, False)
        
    @staticmethod    
    def openAndLoadFile(layers, ft, box, saveDir, gv):
        """
        Use dialog to open file of FileType ft chosen by user, 
        add a layer for it if necessary, 
        copy files to saveDir, write path to box,
        and return file path and layer.
        """
        settings = QSettings()
        if settings.contains('/QSWAT/LastInputPath'):
            path = unicode(settings.value('/QSWAT/LastInputPath'))
        else:
            path = ''
        title = QSWATUtils.trans(FileTypes.title(ft))
        inFileName = QFileDialog.getOpenFileName(None, title, path, FileTypes.filter(ft))
        if inFileName:
            settings.setValue('/QSWAT/LastInputPath', os.path.dirname(unicode(inFileName)))
            # copy to saveDir if necessary
            inInfo = QFileInfo(inFileName)
            inDir = inInfo.absoluteDir()
            outDir = QDir(saveDir)
            if inDir != outDir:
                inFile = inInfo.fileName()
                if inFile == 'sta.adf' or inFile == 'hdr.adf':
                    # ESRI grid - whole directory will be copied to saveDir
                    inDirName = inInfo.dir().dirName()
                    if ft == FileTypes._DEM:
                        # will be converted to .tif, so make a .tif name
                        outFileName = QSWATUtils.join(saveDir, inDirName) + '.tif'
                    else:
                        outFileName = QSWATUtils.join(QSWATUtils.join(saveDir, inDirName), inFile)
                else:
                    outFileName = QSWATUtils.join(saveDir, inFile)
                    if ft == FileTypes._DEM:
                        # will be converted to .tif, so convert to .tif name
                        outFileName = os.path.splitext(outFileName)[0] + '.tif'
                if ft == FileTypes._DEM:
                    # use GDAL CreateCopy to ensure result is a GeoTiff
                    inDs = gdal.Open(inFileName)
                    driver = gdal.GetDriverByName('GTiff')
                    # QSWATUtils.information('Creating {0}'.format(outFileName), gv.isBatch)
                    outDs = driver.CreateCopy(outFileName, inDs, 0)
                    if outDs is None:
                        QSWATUtils.error('Failed to create dem in geoTiff format', gv.isBatch)
                        return (None, None)
                    inDs = None
                    outDs = None
                    # may not have got projection information, so if any copy it
                    QSWATUtils.copyPrj(inFileName, outFileName)
                else:
                    QSWATUtils.copyFiles(inInfo, saveDir)
            else:
                outFileName = inFileName
            # this function will add layer if necessary
            layer = QSWATUtils.getLayerByFilename(layers, outFileName, ft, gv, True)[0]
            if not layer:
                return (None, None)
            # if no .prj file, try to create one
            # this is needed, for example, when DEM is created from ESRI grid
            # or if DEM is made by clipping
            QSWATUtils.writePrj(outFileName, layer)
            box.setText(outFileName)
            return (outFileName, layer)
        else: return (None, None)
        
    @staticmethod
    def getFeatureByValue(layer, indx, val):
        """Return feature in features whose attribute with index indx has value val."""
        for f in layer.getFeatures():
            v = f.attributes()[indx]
            if v == val:
                # QSWATUtils.loginfo('Looking for {0!s} found {1!s}: finished'.format(val, v))
                return f
            # QSWATUtils.loginfo('Looking for {0!s} found {1!s}: still looking'.format(val, v))
        return None
        
    @staticmethod
    def writePrj(fileName, layer):
        """If no .prj file exists for fileName, try to create one from the layer's crs."""
        prjFile = os.path.splitext(fileName)[0] + '.prj'
        if os.path.exists(prjFile):
            return
        try:
            srs = layer.crs()
            wkt = srs.toWkt()
            if not wkt:
                raise ValueError('Could not make WKT from CRS.')
            with fileWriter(prjFile) as fw:
                fw.writeLine(wkt)
        except Exception:
            QSWATUtils.information("Unable to make .prj file for {0}.  You may need to set this map's projection manually".format(fileName), False)
        
    @staticmethod
    def makeCurrent(string, combo):
        """Add string to combo box if not already present, and make it current."""
        index = combo.findText(string)
        if index < 0:
            combo.addItem(string)
            combo.setCurrentIndex(combo.count() - 1)
        else:
            combo.setCurrentIndex(index) 
        
    @staticmethod
    def parseSlopes(string):
        """
        Parse a slope limits string to list of intermediate limits.
        For example '[min, a, b, max]' would be returned as [a, b].
        """
        slopeLimits = []
        nums = string.split(',')
        # ignore first and last
        for i in range(1, len(nums) - 1):
            slopeLimits.append(float(nums[i]))
        return slopeLimits
        
    @staticmethod
    def slopesToString(slopeLimits):
        """
        Return a slope limits string made from a string of intermediate limits.
        For example [a, b] would be returned as '[0, a, b, 9999]'.
        """
        str1 = '[0, '
        for i in slopeLimits:
            # lose the decimal point if possible
            d = int(i)
            if i == d:
                str1 += ('{0!s}, '.format(d))
            else:
                str1 += ('{0!s}, '.format(i))
        return str1 + '9999]'
    
    @staticmethod
    def insertIntoSortedList(val, vals, unique):
        """
        Insert val into assumed upward sorted list vals.  
        If unique is true and val already in vals do nothing.
        Return true if insertion made.
        
        Note function is polymorphic: used for lists of integers and lists of strings
        """
        for index in xrange(len(vals)):
            nxt = vals[index]
            if nxt == val:
                if unique:
                    return False
                else:
                    vals.insert(index, val)
                    return True
            if nxt > val:
                vals.insert(index, val)
                return True
        vals.append(val)
        return True
            
    @staticmethod
    def date():
        """Retun tpday's date as day month year."""
        return datetime.date.today().strftime('%d %B %Y')
    
    @staticmethod
    def time():
        """Return the time now as hours.minutes."""
        return datetime.datetime.now().strftime('%H.%M')
    
    @staticmethod
    def fileBase(SWATBasin, relhru):
        """
        Return the string used to name SWAT input files 
        from basin and relative HRU number.
        """
        return '{0:05d}{1:04d}'.format(SWATBasin, relhru)
    
    @staticmethod
    def getSlsubbsn(meanSlope):
        """Estimate the average slope length in metres from the mean slope."""     
        if meanSlope < 0.01: return 120
        elif meanSlope < 0.02: return 100
        elif meanSlope < 0.03: return 90
        elif meanSlope < 0.05: return 60
        else: return 30
        
    @staticmethod
    def setXMLValue(xmlFile, tag, attName, attVal, tagVal):
        """
        In xmlFile, sets the value of node tag with attName equal to attVal to tagVal.
        
        Return true and empty string if ok, else false and an error string.
        """
        doc = QDomDocument()
        f = QFile(xmlFile)
        done = False
        if f.open(QIODevice.ReadWrite):
            if doc.setContent(f):
                tagNodes = doc.elementsByTagName(tag)
                for i in xrange(tagNodes.length()):
                    tagNode = tagNodes.item(i)
                    atts = tagNode.attributes()
                    key = atts.namedItem(attName)
                    if key is not None:
                        att = key.toAttr()
                        val = att.value()
                        if val == attVal:
                            textNode = tagNode.firstChild().toText()
                            textNode.setNodeValue(tagVal)
                            newVal = tagNode.firstChild().toText().nodeValue()
                            if newVal != tagVal:
                                return False, u'found new XML value of {0} instead of {1}'.fomat(newVal, val)
                            done = True
                            break
                if not done:
                    return False, u'Failed to find {0} node with {1}={2} in {3}'.format(tag, attName, attVal, xmlFile)
            else:
                return False, u'Failed to read XML file {0}'.format(xmlFile)
        else:
            return False, u'Failed to open XML file {0}'.format(xmlFile)
        f.resize(0)
        strm = QTextStream(f)
        doc.save(strm, 4)
        f.close()
        return True, u''
    
    
class fileWriter:
    
    """
    Class effectively extending writer
    to support changing the end-of-line character for Windows/Linux.
    Also supplies a writeLine method
    """
    
    # should be automatically changed for Windows, but isn't
    _END_LINE = os.linesep # '\r\n' for Windows
    
    def __init__(self, path):
        """Initialise class variables."""
        ## writer
        self.writer = open(path, 'w')
        ## write method
        self.write = self.writer.write
    
    def writeLine(self, string):
        """Write string plus end-of-line."""
        self.writer.write(string + fileWriter._END_LINE)
        
    def __enter__(self):
        """Return self."""
        return self
        
    def __exit__(self, typ, value, traceback):  # @UnusedVariable
        """Close."""
        self.writer.close()
        
class FileTypes:
    
    """File types for various kinds of file that will be loaded, 
    and utility functions.
    """
    
    _DEM = 0
    _MASK = 1
    _BURN = 2
    _OUTLETS = 3
    _STREAMS = 4
    _SUBBASINS = 5
    _LANDUSES = 6
    _SOILS = 7
    _SLOPEBANDS = 8
    _REACHES = 9
    _WATERSHED = 10
    _EXISTINGSUBBASINS = 11
    _EXISTINGWATERSHED = 12
    _HILLSHADE = 13
    _GRID = 14
    _GRIDSTREAMS = 15
    
    @staticmethod
    def filter(ft):
        """Return filter for open file dialog according to file type."""
        if ft == FileTypes._DEM or ft == FileTypes._LANDUSES or ft == FileTypes._SOILS or ft == FileTypes._HILLSHADE:
            return QgsProviderRegistry.instance().fileRasterFilters()
        elif ft == FileTypes._MASK:
            return 'All files (*)' # TODO: use dataprovider.fileRasterFilters + fileVectorFilters
        elif ft == FileTypes._BURN or ft == FileTypes._OUTLETS or \
                    ft == FileTypes._STREAMS or ft == FileTypes._SUBBASINS or \
                    ft == FileTypes._REACHES or ft == FileTypes._WATERSHED or \
                    ft == FileTypes._EXISTINGSUBBASINS or ft == FileTypes._EXISTINGWATERSHED or \
                    ft == FileTypes._GRID or ft == FileTypes._GRIDSTREAMS:
            return QgsProviderRegistry.instance().fileVectorFilters()
        
    @staticmethod
    def legend(ft):
        """Legend entry string for file type ft."""
        if ft == FileTypes._DEM:
            return 'DEM'
        elif ft == FileTypes._MASK:
            return 'Mask'
        elif ft == FileTypes._BURN:
            return 'Stream burn-in'
        elif ft == FileTypes._OUTLETS:
            return 'Inlets/outlets'
        elif ft == FileTypes._STREAMS:
            return 'Streams'
        elif ft == FileTypes._SUBBASINS or ft == FileTypes._EXISTINGSUBBASINS:
            return 'Subbasins'
        elif ft == FileTypes._LANDUSES:
            return 'Landuses'
        elif ft == FileTypes._SOILS:
            return 'Soils'
        elif ft == FileTypes._SLOPEBANDS:
            return 'Slope bands'
        elif ft == FileTypes._WATERSHED or ft == FileTypes._EXISTINGWATERSHED:
            return QSWATUtils._WATERSHEDLEGEND
        elif ft == FileTypes._REACHES:
            return 'Reaches'
        elif ft == FileTypes._HILLSHADE:
            return QSWATUtils._HILLSHADELEGEND
        elif ft == FileTypes._GRID:
            return QSWATUtils._GRIDLEGEND
        elif ft == FileTypes._GRIDSTREAMS:
            return QSWATUtils._GRIDSTREAMSLEGEND
        
    @staticmethod
    def styleFile(ft):
        """.qml file, if any, for file type ft."""
        if ft == FileTypes._DEM:
            return None
        elif ft == FileTypes._MASK:
            return None
        elif ft == FileTypes._BURN:
            return None
        elif ft == FileTypes._OUTLETS:
            return 'outlets.qml'
        elif ft == FileTypes._STREAMS or ft == FileTypes._REACHES:
            return 'stream.qml'
        elif ft == FileTypes._SUBBASINS or ft == FileTypes._WATERSHED:
            return 'wshed.qml'
        elif ft == FileTypes._EXISTINGSUBBASINS or ft == FileTypes._EXISTINGWATERSHED:
            return 'existingwshed.qml'
        elif ft == FileTypes._GRID:
            return 'grid.qml'
        elif ft == FileTypes._GRIDSTREAMS:
            return 'gridstreams.qml'
        elif ft == FileTypes._LANDUSES:
            return None
        elif ft == FileTypes._SOILS:
            return None
        elif ft == FileTypes._SLOPEBANDS:
            return None
        elif ft == FileTypes._HILLSHADE:
            return None

    @staticmethod
    def title(ft):
        """Title for open file dialog for file type ft."""
        if ft == FileTypes._DEM:
            return 'Select DEM'
        elif ft == FileTypes._MASK:
            return 'Select mask'
        elif ft == FileTypes._BURN:
            return 'Select stream reaches shapefile to burn-in'
        elif ft == FileTypes._OUTLETS:
            return 'Select inlets/outlets shapefile'
        elif ft == FileTypes._STREAMS:
            return 'Select stream reaches shapefile'
        elif ft == FileTypes._SUBBASINS or ft == FileTypes._EXISTINGSUBBASINS:
            return 'Select watershed shapefile'
        elif ft == FileTypes._LANDUSES:
            return 'Select landuses file'
        elif ft == FileTypes._SOILS:
            return 'Select soils file'
        elif ft == FileTypes._SLOPEBANDS or ft == FileTypes._REACHES or ft == FileTypes._WATERSHED or \
                ft == FileTypes._EXISTINGWATERSHED or ft == FileTypes._HILLSHADE or ft == FileTypes._GRID:
            return None
        
    @staticmethod
    def colourFun(ft):
        """Layer colouring function for raster layer of file type ft."""
        if ft == FileTypes._DEM:
            return FileTypes.colourDEM
        elif ft == FileTypes._LANDUSES:
            return FileTypes.colourLanduses
        elif ft == FileTypes._SOILS:
            return FileTypes.colourSoils
        elif ft == FileTypes._SLOPEBANDS:
            return FileTypes.colourSlopes
        else:
            return None

    @staticmethod
    def colourDEM(layer, _):
        """Layer colouring function for DEM."""
        layer.setDrawingStyle('SingleBandPseudoColor')
        stats = layer.dataProvider().bandStatistics(1, QgsRasterBandStats.Min | QgsRasterBandStats.Max)
        minVal = int(stats.minimumValue + 0.5)
        maxVal = int(stats.maximumValue + 0.5)
        mean = (minVal + maxVal) / 2
        s1 = str((minVal * 2 + maxVal) / 3)
        s2 = str((minVal + maxVal * 2) / 3)
        item0 = QgsColorRampShader.ColorRampItem(minVal, QColor(10, 100, 10), str(minVal) + ' - ' + s1)
        item1 = QgsColorRampShader.ColorRampItem(mean, QColor( 153, 125, 25), s1 + ' - ' + s2)
        item2 = QgsColorRampShader.ColorRampItem(maxVal, QColor(255, 255, 255), s2 + ' - ' + str(maxVal))
        fcn = QgsColorRampShader(minVal, maxVal)
        fcn.setColorRampType(QgsColorRampShader.INTERPOLATED)
        fcn.setColorRampItemList([item0, item1, item2])
        layer.renderer().shader().setRasterShaderFunction(fcn)
        layer.triggerRepaint()
        
    @staticmethod
    def colourLanduses(layer, db):
        """Layer colouring function for landuse grid."""
        layer.setDrawingStyle('SingleBandPseudoColor')
        items = []
        for i in db.landuseVals:
            luse = db.getLanduseCode(i)
            item = QgsColorRampShader.ColorRampItem(int(i), FileTypes.randColor(), luse)
            items.append(item)
        fcn = QgsColorRampShader()
        fcn.setColorRampType(QgsColorRampShader.DISCRETE)
        fcn.setColorRampItemList(items)
        layer.renderer().shader().setRasterShaderFunction(fcn)
        layer.triggerRepaint()
    
    @staticmethod
    def colourSoils(layer, db):
        """Layer colouring function for soil grid."""
        layer.setDrawingStyle('SingleBandPseudoColor')
        items = []
        for i in db.soilVals:
            soilName = str(i) if db.useSSURGO else db.getSoilName(i)
            item = QgsColorRampShader.ColorRampItem(int(i), FileTypes.randColor(), soilName)
            items.append(item)
        fcn = QgsColorRampShader()
        fcn.setColorRampType(QgsColorRampShader.DISCRETE)
        fcn.setColorRampItemList(items)
        layer.renderer().shader().setRasterShaderFunction(fcn)
        layer.triggerRepaint()
        
    @staticmethod
    def colourSlopes(layer, db):
        """Layer colouring for slope bands grid."""
        layer.setDrawingStyle('SingleBandPseudoColor')
        items = []
        numItems = len(db.slopeLimits) + 1
        for n in range(numItems):
            colour = int(5 + float(245) * (numItems - 1 - n) / (numItems - 1))
            item = QgsColorRampShader.ColorRampItem(n, QColor(colour, colour, colour), db.slopeRange(n))
            items.append(item)
        fcn = QgsColorRampShader()
        fcn.setColorRampType(QgsColorRampShader.DISCRETE)
        fcn.setColorRampItemList(items)
        layer.renderer().shader().setRasterShaderFunction(fcn)
        layer.triggerRepaint()
    
    @staticmethod
    def randColor():
        """Return random QColor."""
        return QColor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    
