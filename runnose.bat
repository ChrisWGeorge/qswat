set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\qgis\python
rem QGIS binaries
set PATH=%OSGEO4W_ROOT%\apps\qgis\bin;%OSGEO4W_ROOT%\apps\qgis\python;;%OSGEO4W_ROOT%\bin;%OSGEO4W_ROOT%\apps\Python27\DLLs;%OSGEO4W_ROOT%\apps\Python27\Scripts
rem disable QGIS console messages
set QGIS_DEBUG=-1

rem default QGIS plugins
set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\qgis\python\plugins
rem user installed plugins
set PYTHONPATH=%PYTHONPATH%;%USERPROFILE%\.qgis2\python\plugins

rem Convert double backslashes to single
set PATH=%PATH:\\=\%
set PYTHONPATH=%PYTHONPATH:\\=\%

rem can't seem to install nose any more
rem nosetests.exe

python -m unittest test_qswat
python -m unittest test_polygonizeInC2