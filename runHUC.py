# -*- coding: utf-8 -*-
"""
/***************************************************************************
 QSWAT
                                 A QGIS plugin
 Run HUC project
                              -------------------
        begin                : 2014-07-18
        copyright            : (C) 2014 by Chris George
        email                : cgeorge@mcmaster.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""


from qgis.core import * # @UnusedWildImport
from PyQt4.QtCore import * # @UnusedWildImport
import atexit
import sys
import os
import glob
from osgeo import gdal, ogr  # type: ignore

import qswat
from delineation import Delineation
from hrus import HRUs
import traceback


osGeo4wRoot = os.getenv('OSGEO4W_ROOT')
QgsApplication.setPrefixPath(osGeo4wRoot + r'\apps\qgis', True)

QgsApplication.initQgis()

# create a new application object
# without this importing processing causes the following error:
# QWidget: Must construct a QApplication before a QPaintDevice
app = QgsApplication([], True)


atexit.register(QgsApplication.exitQgis)

class DummyInterface(object):
    """Dummy iface."""
    def __getattr__(self, *args, **kwargs):
        """Dummy function."""
        def dummy(*args, **kwargs):
            return self
        return dummy
    def __iter__(self):
        """Dummy function."""
        return self
    def next(self):
        """Dummy function."""
        raise StopIteration
    def layers(self):
        """Simulate iface.legendInterface().layers()."""
        return QgsMapLayerRegistry.instance().mapLayers().values()
iface = DummyInterface()

QCoreApplication.setOrganizationName('QGIS')
QCoreApplication.setApplicationName('QGIS2')

class runHUC():
    
    """Run HUC14/12/10 project."""
    
    def __init__(self, projDir):
        """Initialize"""
        ## project directory
        self.projDir = projDir
        ## QSWAT plugin
        self.plugin = qswat.QSwat(iface)
        ## QGIS project
        self.proj = QgsProject.instance()
        self.proj.read(QFileInfo(self.projDir + '.qgs'))
        self.plugin.setupProject(self.proj, True, isHUC=True)
        ## main dialogue
        self.dlg = self.plugin._odlg
        ## delineation object
        self.delin = None
        ## hrus object
        self.hrus = None
        # Prevent annoying "error 4 .shp not recognised" messages.
        # These should become exceptions but instead just disappear.
        # Safer in any case to raise exceptions if something goes wrong.
        gdal.UseExceptions()
        ogr.UseExceptions()
        
    def runProject(self):
        """Run QSWAT project."""
        gv = self.plugin._gv
        self.delin = Delineation(iface, gv, self.plugin._demIsProcessed)
        self.delin._dlg.tabWidget.setCurrentIndex(1)
        self.delin._dlg.selectDem.setText(self.projDir + '/Source/dem.tif')
        self.delin._dlg.selectWshed.setText(self.projDir + '/Watershed/Shapes/demwshed.shp')
        self.delin._dlg.selectNet.setText(self.projDir + '/Watershed/Shapes/channels.shp')
        self.delin._dlg.selectExistOutlets.setText(self.projDir + '/Watershed/Shapes/points.shp')
        self.delin._dlg.recalcButton.setChecked(False)  # want to use length field in channels shapwefile
        self.delin._dlg.numProcesses.setValue(0)
        gv.useGridModel = False
        gv.existingWshed = True
        self.delin.runExisting()
        self.delin.finishDelineation()
        self.delin._dlg.close()
        self.hrus = HRUs(iface, gv, self.dlg.reportsBox)
        self.hrus.init()
        hrudlg = self.hrus._dlg
        self.hrus.landuseFile = self.projDir + '/Source/crop/landuse.tif'
        self.hrus.landuseLayer = QgsRasterLayer(self.hrus.landuseFile, 'landuse')
        self.hrus.soilFile = self.projDir + '/Source/soil/soil.tif'
        self.hrus.soilLayer = QgsRasterLayer(self.hrus.soilFile, 'soil')
        #landCombo = hrudlg.selectLanduseTable
        #landIndex = landCombo.findText('nlcd2001_landuses')
        #landCombo.setCurrentIndex(landIndex)
        self.hrus.landuseTable = 'nlcd2001_landuses'
        # hrudlg.SSURGOButton.setChecked(True)
        gv.db.useSSURGO = True
        if not self.hrus.readFiles():
            hrudlg.close()
            return
        hrudlg.filterLanduseButton.setChecked(True)
        hrudlg.percentButton.setChecked(True)
        hrudlg.landuseVal.setText('0')
        self.hrus.setLanduseThreshold()
        hrudlg.soilVal.setText('0')
        self.hrus.setSoilThreshold()
        hrudlg.slopeVal.setText('0')
        gv.elevBandsThreshold = 500
        gv.numElevBands = 5
        self.hrus.calcHRUs()
        hrudlg.close()
        
    def addInlet(self, inletId):
        """Add watershed inlet."""
        gv = self.plugin._gv
        db = gv.db
        # get point from demnet.shp
        pointsFile = gv.projDir + '/Watershed/Shapes/points.shp'
        pointsLayer = QgsVectorLayer(pointsFile, 'points', 'ogr')
        exp = QgsExpression('"ID" = {0}'.format(inletId))
        point = next(pointsLayer.getFeatures(QgsFeatureRequest(exp)))
        pointXY = point.geometry().asPoint()
        pointll = gv.topo.pointToLatLong(pointXY)
        # to find subbasin, find zero length channel with inletId as LINKNO, and then subbasin of its downstream channel
        channelsFile = gv.projDir + '/Watershed/Shapes/demnet.shp'
        channelsLayer = QgsVectorLayer(channelsFile, 'channels', 'ogr')
        dsLinkNoIndex = channelsLayer.fieldNameIndex('DSLINKNO')
        wsnoIndex = channelsLayer.fieldNameIndex('WSNO')
        exp1 = QgsExpression('"LINKNO" = {0}'.format(inletId))
        channel = next(channelsLayer.getFeatures(QgsFeatureRequest(exp1).setFlags(QgsFeatureRequest.NoGeometry)))
        dsLinkNo = channel[dsLinkNoIndex]
        exp2 = QgsExpression('"LINKNO" = {0}'.format(dsLinkNo))
        channel = next(channelsLayer.getFeatures(QgsFeatureRequest(exp2).setFlags(QgsFeatureRequest.NoGeometry)))
        wsno = channel[wsnoIndex]
        with db.connect() as conn:
            cursor = conn.cursor()
            # read MonitoringPoint table to gat max OBJECTID
            table = 'MonitoringPoint'
            maxId = 0
            sql = db.sqlSelect(table, 'OBJECTID', '', '')
            for row in cursor.execute(sql):
                maxId = max(maxId, int(row[0]))
            # add inlet point
            maxId += 1
            POINTID  = 0
            HydroID = maxId + 400000
            OutletID = maxId + 100000
            SWATBasin = wsno
            GRID_CODE = SWATBasin
            elev = 0 # only used for weather gauges
            name = '' # only used for weather gauges
            typ = 'W'
            sql2 = 'INSERT INTO ' + table + ' VALUES(?,0,?,?,?,?,?,?,?,?,?,?,?,?)'
            cursor.execute(sql2, maxId, POINTID, GRID_CODE,
                       float(pointXY.x()), float(pointXY.y()), float(pointll.y()), float(pointll.x()), 
                       float(elev), name, typ, SWATBasin, HydroID, OutletID)
            
        
if __name__ == '__main__':
    # for arg in sys.argv:
    #     print('Argument: {0}'.format(arg)) 
    if len(sys.argv) < 3:
        print('You must supply a directory and 0 or a inlet number as argument')
        exit()
    direc = sys.argv[1]
    # print('direc is {0}'.format(direc))
    inletId = int(sys.argv[2])
    # print('inletId is {0}'.format(inletId))
    if inletId > 0:
        # add inlet point with this id to MonitoringPoint table of existing project
        print('Adding inlet {0} to project {1}'.format(inletId, direc))
        huc = runHUC(direc)
        huc.addInlet(inletId)
    else:
        pattern = direc + '/huc*'
        for d in glob.iglob(pattern):
            if os.path.isdir(d):
                # if this message is changed HUC12/14Models main function will need changing since it selects HUC from this message
                print('Running project {0}'.format(d))
                try:
                    huc = runHUC(d)
                    huc.runProject()
                    print('Completed project {0}'.format(d))
                except Exception:
                    print('ERROR: exception: {0}'.format(traceback.format_exc()))
                sys.stdout.flush()
    app.exitQgis()
    app.exit()
    del app    
        