set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\qgis\python
rem QGIS binaries
set PATH=%PATH%;%OSGEO4W_ROOT%\apps\qgis\bin;%OSGEO4W_ROOT%\apps\qgis\python 
rem disable QGIS console messages
set QGIS_DEBUG=-1

rem default QGIS plugins
set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\qgis\python\plugins
rem user installed plugins
set PYTHONPATH=%PYTHONPATH%;%USERPROFILE%\.qgis2\python\plugins

rem Python version of polygonize
python -m unittest -v test_polygonize
rem Cython version of polygonize
python -m unittest -v test_polygonizeInC
rem Cython version 2 of polygonize
python -m unittest -v test_polygonizeInC2
